(function(angular) {
  'use strict';

  angular
    .module('app.services.speakerService', [])
    .factory('speakerService', function speakerService($http, SETTINGS, conferenceCache) {
      var URLS = {
        INDEX: SETTINGS.LIVE_API_URL + 'conference_speakers',
        FEATURED: SETTINGS.LIVE_API_URL + 'conference_speakers',
        GROUPS: SETTINGS.LIVE_API_URL + 'conference_speaker_groups',
        TYPE: SETTINGS.LIVE_API_URL + 'conference_speakers',
      };

      var OPTIONS = {
        cache: conferenceCache
      };

      var ACTIONS = {
        INDEX: function(callback) {
          $http
            .get(URLS.INDEX, OPTIONS)
            .success(function(response){
              callback(response.data);
            })
          ;
        },
        GROUPS: function(callback) {
          $http
            .get(URLS.GROUPS, OPTIONS)
            .success(function(response){
              callback(response.data);
            })
          ;
        },
        SHOW: function(id, callback) {
          $http
            .get(URLS.SHOW, OPTIONS)
            .success(function(response) {
              var result = null;

              response.data.forEach(function(item) {
                if (item.id == id) {
                  result = item;
                }
              });

              callback(result);
            })
          ;
        },
        FEATURED: function(limit, callback) {
          var url = URLS.FEATURED;

          if (limit) {
            url = url + '?filter[promote][value]=1&range=' + (limit + 1);
          }

          $http
            .get(url, OPTIONS)
            .success(function(response) {
              callback(response.data);
            })
          ;
        },
        TYPE: function(type, limit, callback) {
          $http
            .get(URLS.TYPE, OPTIONS)
            .success(function(response) {
              var result = [];
              var count  = 0;

              response.data.forEach(function(item) {
                if (count < limit && item.speaker_group != null && item.speaker_group.name == type) {
                  result.push(item);
                  count++;
                }
              });
              callback(result);
            })
          ;
        }
      };

      return {
        getAll:      ACTIONS.INDEX,
        getOne:      ACTIONS.SHOW,
        getFeatured: ACTIONS.FEATURED,
        getType:     ACTIONS.TYPE,
        getGroups:   ACTIONS.GROUPS
      }
    })
  ;
})(window.angular);
