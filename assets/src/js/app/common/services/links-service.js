(function(angular) {
  'use strict';

  angular
    .module('app.services.linksService', [])
    .factory('linksService', function menuService($http, SETTINGS, conferenceCache) {
      var URLS = {
        INDEX: SETTINGS.LIVE_API_URL + 'conference_links'
      };

      var OPTIONS = {
        cache: conferenceCache
      };

      var getAll = function(callback) {
        $http
          .get(URLS.INDEX, OPTIONS)
          .success(function(response) {            
            callback(response.data);
          })
        ;
      };

      /**
       * Return the public interface.
       */
      return {
        getAll: getAll
      };
    })
  ;
})(window.angular);
