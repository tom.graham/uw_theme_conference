(function(angular) {
  'use strict';

  angular
    .module('app.services.advertisementService', [])
    .factory('advertisementService', function advertisementService($http, SETTINGS, conferenceCache) {
      var URLS = {
        INDEX: SETTINGS.LIVE_API_URL + 'conference_advertisements'
      };

      var OPTIONS = {
        cache: conferenceCache
      };

      var ACTIONS = {
        INDEX: function(callback) {
          $http
            .get(URLS.INDEX, OPTIONS)
            .success(function(response) {
              var results = [];

              if (response.data && response.data.length > 0) {
                results = response.data;
              }

              callback(results);
            })
          ;
        }
      };

      // Return the public interface
      return {
        getAll: ACTIONS.INDEX
      };
    })
  ;
})(window.angular);
