(function(angular, $) {
  'use strict';

  angular
    .module('app.services.ckeditorSocialMediaService', [])
    .factory('ckeditorSocialMediaService', function ckeditorSocialMediaService($compile, $rootScope, $timeout) {
      var parseLiveStream = function parseLiveStream() {
        // Live stream
        if ($('div.cklivestream')) {
          $('div.cklivestream').hide().each(function() {
            var lswidth = 500;
            var lsheight = 307;
            var lsfitwidth = $(this).parent().width();
            var lsfitheight = Math.floor(lsfitwidth / lswidth * lsheight);
            var lsurl = encodeURIComponent($(this).data('username'));
            //note: using HTTP for <a href> because Livestream has certificate problems.
            $(this).before('<iframe width="'+lsfitwidth+'" height="'+lsfitheight+'" src="//cdn.livestream.com/embed/'+lsurl+'?layout=4&width='+lsfitwidth+'&height='+lsfitheight+'&color=0x'+$rootScope.meta().settings.ckeditor_socialmedia.livestream_color+'&iconColor=0x'+$rootScope.meta().settings.ckeditor_socialmedia.livestream_icon_color+'&iconColorOver=0x'+$rootScope.meta().settings.ckeditor_socialmedia.livestream_icon_color_over+'" style="border:0;outline:0" frameborder="0" scrolling="no"></iframe><div><a href="http://www.livestream.com/'+lsurl+'">Watch video on Livestream</a></div>');
          });
        }
      };

      var parseFacebook = function parseFacebook() {
        if ( $('div.ckfacebook')) {
          $('div.ckfacebook').hide().each(function() {
            var fbwidth = $(this).parent().width();
            var fburl = encodeURIComponent('/'+$(this).data('username'));
            $(this).before('<iframe src="//www.facebook.com/plugins/likebox.php?href='+fburl+'&amp;width='+fbwidth+'&amp;height=350&amp;colorscheme='+$rootScope.meta().settings.ckeditor_socialmedia.facebook_colorscheme+'&amp;show_faces=false&amp;border_color&amp;stream=true&amp;header=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:'+fbwidth+'px; height:350px;" allowTransparency="true"></iframe>');
          });
        }
      };

      var parseVideos = function parseVideos() {
        if ( $('div.uw_video-embed') ) {
          $('div.uw_video-embed').fitVids();
        }
        if ( $('div.vimeoWrapper') ) {
          $('div.vimeoWrapper').fitVids();
        }
      };

      var parseNewTwitter = function parseNewTwitter() {
        if( !($("div.cktwitter")) ) {
          return;
        }
        $('blockquote.twitter-tweet').hide();
        $('div.cktwitter[data-widget-id] a').hide().attr('width',$('div.cktwitter').parent().width());

        $timeout(function() {
          // var html = '<script id="twitter-wjs" src="//platform.twitter.com/widget.js" type="text/lazyload"></script>';
          // var scope = $rootScope;
          // html = $compile(html)(scope);
          // $('body').append(html);


          !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");

          //if a new Twitter widget failed to load after 2 seconds, show the link (if Twitter does eventually load, it will hide the link itself)
          setTimeout(function() {
            //standard Twitter widgets

            $.each($('div.cktwitter[data-widget-id]'), function() {
              var $iframe = $('iframe', this);
              if (!$iframe.length || $iframe.contents().find('body').html() == '') {
                $(this).closest('div').find('a').show();
              }
            });
            //embedded Tweets (Twitter removes the blockquote if it succeeds)
            $('blockquote.twitter-tweet').show();
          }, 2000);
        }, 1000);
      };

      var parse = function parse() {
        parseLiveStream();
        parseFacebook();
        parseNewTwitter();
        parseVideos();
      };

      /**
       * Return the public interface.
       */
      return {
        parse: parse
      }
    })
  ;
})(window.angular, window.jQuery);
