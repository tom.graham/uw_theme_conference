(function(angular) {
  'use strict';

  angular
    .module('app.services.blogService', [])
    .factory('blogService', function blogService($http, SETTINGS, conferenceCache) {
      var URLS = {
        INDEX:    SETTINGS.LIVE_API_URL + 'conference_blog',
        SHOW:     SETTINGS.LIVE_API_URL + 'conference_blog/blog/post',
        FEATURED: SETTINGS.LIVE_API_URL + 'conference_blog?range=1&filter[sticky][value]=1',
        CATEGORIES: SETTINGS.LIVE_API_URL + 'conference_blog_categories',
      };

      var OPTIONS = {
        cache: conferenceCache
      };

      var getQueryParams = function(params) {
        var queryParams = [];

        if (params.limit) {
          queryParams.push('range=' + params.limit);
        }

        if (params.page) {
          queryParams.push('page=' + params.page);
        }

        if (params.exclude) {
          queryParams.push('filter[id][value]=' + params.exclude);
          queryParams.push('filter[id][operator]=!=');
        }

        return queryParams.join('&');
      };

      var ACTIONS = {
        INDEX: function(params, callback) {

          var url = URLS.INDEX;

          if (params) {
            url += '?' + getQueryParams(params);
          }

          $http
            .get(url, OPTIONS)
            .success(function(response) {
              callback(response); // Send back all data because we need the count for pagination.
            })
          ;
        },

        RECENT: function(limit, callback) {
          $http
            .get(URLS.INDEX + '?range=' + limit)
            .success(function(response) {
              callback(response.data);
            })
          ;
        },

        BY_CATEGORY: function(params, name, callback) {
          var url = URLS.INDEX + '?';

          if (params) {
            url += getQueryParams(params);
          }

          url += '&filter[categories][value]=' + encodeURI(name);

          $http
            .get(url, OPTIONS)
            .success(function(response) {
              callback(response); // Send back all data because we need the count for pagination.
            })
          ;
        },

        BY_TERM: function(params, term, filters, callback) {
          var url = URLS.INDEX + '?';

          if (params) {
            url += getQueryParams(params);
          }

          filters = (filters) ? filters : {};
          url += '&filter[title][value]=' + encodeURI(term);

          if (filters.category) {
            url += '&filter[categories][value]=' + encodeURI(filters.category);
          }

          $http
            .get(url, OPTIONS)
            .success(function(response) {
              callback(response); // Send back all data because we need the count for pagination.
            })
          ;
        },

        SHOW: function(slug, callback) {
          $http
            .get(URLS.SHOW + '/' + slug, OPTIONS)
            .success(function(response) {
              var result = null;

              if (response.data && response.data.length > 0) {
                result = response.data[0];
              }

              callback(result);
            })
            .error(function() {
              callback();
            })
          ;
        },

        FEATURED: function(limit, callback) {
          $http
            .get(URLS.FEATURED)
            .success(function(response) {
              var results = [];
              var count   = 0;

              _.each(response.data, function(item) {
                if (count < limit) {
                  results.push(item);
                  count++;
                }
              });

              callback(results);
            })
          ;
        },

        CATEGORIES: function(callback) {
          $http
            .get(URLS.CATEGORIES)
            .success(function(response) {
              callback(response.data);
            })
          ;
        }
      };

      return {
        getAll: ACTIONS.INDEX,
        getAllByCategory: ACTIONS.BY_CATEGORY,
        getAllByTerm: ACTIONS.BY_TERM,
        getOne: ACTIONS.SHOW,
        getFeatured: ACTIONS.FEATURED,
        getRecent: ACTIONS.RECENT,
        getCategories: ACTIONS.CATEGORIES,
      }
    })
  ;
})(window.angular);
