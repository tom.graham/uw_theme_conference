(function(angular) {
  'use strict'; 

  angular
    .module('app.config.router', [
      'ui.router',
      'app.services.sponsorService'
    ])
    .config(function($stateProvider, $urlRouterProvider, SETTINGS) {
      // Default route if no route matched
      $urlRouterProvider.otherwise('/404');
      // Strip trailing slashes so links with trailing slashes work.
      // From stackoverflow.com/questions/24420578/handling-trailing-slashes-in-angularui-router
      $urlRouterProvider.rule(function($injector, $location) {
        var path = $location.path();
        var hasTrailingSlash = path[path.length-1] === '/';
        if(hasTrailingSlash) {
          //if last charcter is a slash, return the same url without the slash  
          var newPath = path.substr(0, path.length - 1); 
          return newPath; 
        } 
      });
      
      // Abstract controller for the app. Here be global logic.
      $stateProvider
        .state('app', {
          url: '',
          abstract: true,
          views: {
            'sponsors@': {
              templateUrl: SETTINGS.TEMPLATE_URL + '/components/sponsors.tpl.html',
              controller: function($scope, sponsorService) {
                $scope.vm = {};
                $scope.vm.sponsors = [];

                sponsorService
                  .getAll(function(data) {
                    $scope.vm.sponsors = data;
                  })
                ;
              }
            }
          }
        })
      ;
    })
  ;

})(window.angular);
