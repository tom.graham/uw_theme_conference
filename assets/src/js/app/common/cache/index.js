(function(angular) {
  'use strict';

  angular
    .module('app.cache', [
      'app.cache.conferenceCache'
    ])
  ;
})(window.angular);