(function(angular) {
  'use strict';

  angular
    .module('app.directives.advertisement', [])
    .directive('advertisement', function advertisement(SETTINGS) {
      /**
       * The controller for this directive.
       * @param advertisementService {object} The advertisement service.
       * @param $scope {object} The scope service.
       */
      var advertisementDirectiveController = function(advertisementService, $scope) {
        // Initialize the controller
        setInitialState();

        /**
         * Set the initial state of the controller.
         */
        function setInitialState() {
          getAdvertisement();
        }

        /**
         * Get an advertisement to display.
         */
        function getAdvertisement() {
          $scope.advertisements = [];
          $scope.loading = true;
          $scope.error  = null;

          advertisementService.getAll(function(data) {
            if (!data) {
              $scope.error = 'Failed to load advertisement';
            }

            $scope.advertisements = data;
            $scope.loading = false;
          });
        }
      };

      /**
       * Return the public interface for the directive.
       */
      return {
        restrict: 'EA',
        controller: advertisementDirectiveController,
        templateUrl: SETTINGS.TEMPLATE_URL + 'directives/advertisement.tpl.html'
      }
    })
  ;
})(window.angular);
