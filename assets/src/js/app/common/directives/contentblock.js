(function(angular) {
  'use strict';

  angular
    .module('app.directives.contentblock', [])
    .directive('contentblock', function(SETTINGS) {
      /**
       * The controller for this directive.
       * @param {object} $scope - The $scope service
       */
      var contentBlockDirectiveController = function($rootScope, $scope) {
        // Initialize the controller.
        setInitialState();

        /**
         * Set the initial state of the controller.
         */
        function setInitialState() {
          $rootScope.$watch('meta().settings', function(newValue, oldValue) {
            if (newValue && newValue.content_block) {
              $scope.contentBlock = newValue.content_block;
            }
          });

          $scope.$watch('dataclasses', function(newValue, oldValue) {
            $scope.classes = angular.copy(newValue);
          });

          $scope.$watch('datalayout', function(newValue, oldValue) {
            $scope.layout = angular.copy(newValue);
          });
        }
      };

      /**
       * Return the public interface for the directive.
       */
      return {
        restrict: 'EA',
        controller: contentBlockDirectiveController,
        scope: {
          dataclasses: '@',
          datalayout: '@',
        },
        templateUrl: SETTINGS.TEMPLATE_URL + 'directives/contentblock.tpl.html'
      }
    })
  ;
})(window.angular);
