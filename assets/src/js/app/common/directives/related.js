(function(angular) {
  'use strict';

  angular
    .module('app.directives.related', [])
    .directive('related', function(SETTINGS) {
      var controller = function($scope, $stateParams) {
        var vm = this;

        setInitialState();

        function setInitialState() {
          vm.posts = angular.copy(vm.datasource);

          $scope.$watch('vm.datasource', function(newValue, oldValue) {
            vm.posts = angular.copy(vm.datasource);
          });
        }
      };

      return {
        restrict: 'EA',
        scope: {
          datasource: '='
        },
        controller: controller,
        controllerAs: 'vm',
        bindToController: true,
        templateUrl: SETTINGS.TEMPLATE_URL + 'directives/related.tpl.html'
      }
    })
  ;
})(window.angular);
