(function(angular) {
  'use strict';

  angular
    .module('app.directives', [
      'app.directives.advertisement',
      'app.directives.agenda',
      'app.directives.blog',
      'app.directives.contentblock',
      'app.directives.featuredagenda',
      'app.directives.featuredposts',
      'app.directives.featuredspeakers',
      'app.directives.highlights',
      'app.directives.homepage',
      'app.directives.links',
      'app.directives.location',
      'app.directives.mailchimpblock',
      'app.directives.related',
      'app.directives.script',
      'app.directives.speakers',
      'app.directives.speakergroups',
      'app.directives.sponsors',
      'app.directives.tint',
      'app.directives.tintembed',
      'app.directives.tweets',
      'app.directives.videos'
    ])
  ;
})(window.angular);
