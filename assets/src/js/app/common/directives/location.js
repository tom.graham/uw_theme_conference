(function(angular) {
  'use strict';

  angular
    .module('app.directives.location', [])
    .directive('location', function(SETTINGS) {
      /**
       * Controller for the directive.
       * @param {object} - The $rootScope service.
       */
      var locationDirectiveController = function(NgMap, $rootScope, $scope, $timeout) {
        var vm = this;

        setInitialState();

        /**
         * Set the initial state of the controller.
         */
        function setInitialState() {

          $scope.mapLocation   = {};

          $rootScope.$watch('meta().settings', function(newValue, oldValue) {
            if (newValue && newValue.location) {
              $scope.mapLocation = newValue.location;
              console.log($scope.mapLocation);
            }
          });

          $scope.$watch('dataclasses', function(newValue, oldValue) {
            $scope.classes = angular.copy(newValue);
          });

          $scope.$watch('datalayout', function(newValue, oldValue) {
            $scope.layout = angular.copy(newValue);
          });
        }
      };

      /**
       * Return the public interface for the directive.
       */
      return {
        restrict: 'EA',
        controller: locationDirectiveController,
        scope: {
          dataclasses: '@',
          datalayout: '@'
        },
        templateUrl: SETTINGS.TEMPLATE_URL + 'directives/location.tpl.html'
      }
    })
  ;
})(window.angular);
