(function(angular) {

  'use strict'

  angular
    .module('app.directives.highlights', [])
    .directive('highlights', function(SETTINGS) {

      /**
       * The controller for this directive.
       * TODO: Find out why this directive isn't maintaining its isolate scope.
       * @param {object} $scope - The $scope service
       * @param {object} videoService - The videoService
       */
      var highlightsDirectiveController = function($rootScope, $scope, videoService) {
        $rootScope.$watch('meta().settings', function(newValue, oldValue) {
          if (newValue && newValue.videos) {
            $scope.videos = newValue.videos;
          }
        });

        $scope.$watch('dataclasses', function(newValue, oldValue) {
          $scope.classes = angular.copy(newValue);
        });

        $scope.$watch('datalayout', function(newValue, oldValue) {
          $scope.layout = angular.copy(newValue);
        });

        $scope.highlightVideos = [];
        $scope.getVideosPage   = $rootScope.getVideosPage;

        $scope.loading = true;
        videoService
          .getFeatured(4, function(data) {
            $scope.loading = false;
            $scope.highlightVideos = data;
            if($scope.highlightVideos.length == 3){
              $scope.threeUp = true;
            }
            if($scope.highlightVideos.length == 2){
              $scope.twoUp = true;
            }
            if($scope.highlightVideos.length == 1){
              $scope.oneUp = true;
            }
          })
        ;
      }

      return {
        restrict: 'EA',
        scope: {
          dataclasses: '@',
          datalayout: '@'
        },
        controller: highlightsDirectiveController,
        templateUrl: SETTINGS.TEMPLATE_URL + 'directives/highlights.tpl.html'
      }

    })
  ;

})(window.angular);
