(function(angular, _) {
  'use strict';

  angular
    .module('app.directives.homepage', [])
    .directive('homepage', function(SETTINGS, $compile, $rootScope) {
      /**
       * Controller for the directive.
       * @param {object} $scope - The $scope service.
       */
      var homepageDirectiveController = function($scope) {

      }

      /**
       * Link callback for the directive.
       * @param {object} scope - The directive scope.
       * @param {object} element - The directive element.
       * @param {object} attrs - The directive element attributes.
       */
      var homepageDirectiveLink = function(scope, element, attrs) {
        $rootScope.$watch('meta().settings', function(newValue, oldValue) {
          if (newValue) {
            var settings = newValue;
            var widgets  = [
              { name: 'twitter_feed', directive: 'tweets' },
              { name: 'content_block', directive: 'contentblock' },
              { name: 'blog', directive: 'featuredposts' },
              { name: 'speakers', directive: 'featuredspeakers' },
              { name: 'agenda', directive: 'featuredagenda' },
              { name: 'videos', directive: 'highlights' },
              { name: 'links', directive: 'links' },
              { name: 'mailchimp_block', directive: 'mailchimpblock' },
              { name: 'tint', directive: 'tint' },
              { name: 'location', directive: 'location' }
            ];

            var html    = '';
            var columns = 0;

            // Get weights
            _.each(widgets, function(widget) {
              var widgetSettings = settings[widget.name];
              var weight = 0;

              if (widgetSettings && widgetSettings.weight) {
                weight = parseInt(widgetSettings.weight, 10);
              }

              widget.weight = weight;
            });

            // Sort by weight
            widgets = _.sortBy(widgets, 'weight');

            // Keep track of left/right positioning
            var position = 'right';

            // Built HTML in order
            _.each(widgets, function(widget) {
              var widgetSettings = settings[widget.name];

              if (widgetSettings && widgetSettings.enabled && widgetSettings.enabled == 1) {
                var classes = [];
                var layout  = 'full';

                if (widgetSettings.layout == 1) {
                  classes.push('half-mod');
                  layout   = 'half';
                  position = (position == 'right') ? 'left' : 'right';
                  classes.push(position + '-mod');
                } else {
                  classes.push('full-mod');
                }

                // Only apply background colors to full-bleed widgets
                if (layout == 'full' && widgetSettings.background == 1) {
                  classes.push('gray-mod');
                } else if (layout == 'full' && widgetSettings.background == 2) {
                  classes.push('dark-gray-mod');
                } else {
                  classes.push('white-mod');
                }

                if (layout == 'half' && widgetSettings.background == 1) {
                  classes.push('gray-screen-mod');
                } else if (layout == 'half' && widgetSettings.background == 2) {
                  classes.push('dark-gray-screen-mod');
                }

                classes = classes.join(' ');

                if (columns == 1 && layout == 'full') {
                  // In this case we are trying to add a full width column into
                  // the second slot.
                  columns  = 0;
                  position = 'right';

                  // Close the previous row.
                  html += '</div>';

                  // Close the previous container.
                  html += '</div>';
                }

                if (layout == 'half') {
                  if (columns == 0) {
                    // Open the container.
                    html += '<div class="container">';

                    // Open the row.
                    html += '<div class="row">';
                  }

                  // Open the column.
                  html += '<div class="col-md-6">';

                  // Note that we placed a column.
                  columns++;
                }

                html += '<div ' + widget.directive + ' dataclasses="' + classes + '" datalayout="' + layout + '"></div>';

                if (layout == 'half') {
                  // Close the columns
                  html += '</div>';
                }

                if ((layout == 'full' && columns > 0) || (layout == 'half' && columns == 2)) {
                  // Close the row
                  html += '</div>';

                  // Close the container
                  html += '</div>';

                  // Start counting columns again.
                  columns = 0;
                  position = 'right'; // So we always start on left.
                }
              }
            });

            html = $compile(html)(scope);
            element.after(html);
          }
        });
      }

      /**
       * Return the public interface for the directive.
       */
      return {
        restrict: 'EA',
        controller: homepageDirectiveController,
        link: homepageDirectiveLink,
        templateUrl: SETTINGS.TEMPLATE_URL + 'directives/homepage.tpl.html'
      }
    })
  ;
})(window.angular, window._);
