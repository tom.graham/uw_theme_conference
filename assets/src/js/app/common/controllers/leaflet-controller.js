(function(angular, _) {
  'use strict';

  angular
    .module('app.controllers.leafletController', [
      'leaflet-directive'
    ])
    .controller('leafletController', function leafletController(metaService, $rootScope, $scope) {

      $rootScope.$watch('meta().settings', function(newValue, oldValue) {
        if (newValue && newValue.location) {

          // If there is a marker icon, add it.
          if (newValue.location.marker_icon) {
            $scope.markers.mainMarker.icon.iconUrl = newValue.location.marker_icon;
            $scope.markers.mainMarker.icon.iconSize = [20, 32];
            $scope.markers.mainMarker.icon.iconAnchor = [10, 0];
          }

          // Set the lat and long for the map center.
          $scope.location.lat = parseFloat(newValue.location.latitude);
          $scope.location.lng = parseFloat(newValue.location.longitude);

          // Set the lat and long for the marker.
          $scope.markers.mainMarker.lat = parseFloat(newValue.location.latitude);
          $scope.markers.mainMarker.lng = parseFloat(newValue.location.longitude);

          // If there is a name, use it for the for marker title or
          // if there is a title use it for the marker title.
          if (newValue.location.name) {
            $scope.markers.mainMarker.message = newValue.location.name;
          }
          else if (newValue.location.title) {
            $scope.markers.mainMarker.message = newValue.location.title;
          }
        }
      });

      // Set the default location to UW and default zoom.
      $scope.location = {
        lat: 43.4718904,
        lng: -80.5443058,
        zoom: 15
      };

      // Set the tiles to use openstreet map.
      $scope.tiles = {
        url: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      };

      // Set the defaults for the marker.
      $scope.markers = {
        mainMarker: {
          lat: 43.4718904,
          lng: -80.5443058,
          icon: {},
          focus: true,
          draggable: false
        }
      };
    })
  ;
})(window.angular, window._);