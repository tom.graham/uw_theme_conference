(function(angular) {
  'use strict';

  angular
    .module('app.runtime', [
      'app.runtime.scope',
      'app.runtime.xhook'
    ])
  ;
})(window.angular);