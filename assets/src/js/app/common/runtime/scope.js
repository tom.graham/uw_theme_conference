(function(angular) {
  'use strict';

  angular
    .module('app.runtime.scope', [
      'ui.router'
    ])
    .run(function($rootScope, $state, $stateParams) {
      // Assign state to rootScope for convenient access in templates
      $rootScope.$state = $state;

      // Assign state params to rootScope for convenient access in templates
      $rootScope.$stateParams = $stateParams;

      // Assign theme uri for convenient access in templates
      $rootScope.THEME_URI = 'apps/uw_theme_conference/';

      // Hook into state changes
      $rootScope.$on('$stateChangeSuccess', function(event, currentRoute, previousRoute) {
        // Scroll window to top on state change
        window.scrollTo(0, 0);
      });
      $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
            $rootScope.menuShown  = false;
        
      });
    })
  ;
})(window.angular);
