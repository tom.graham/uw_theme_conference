(function(angular) {
  'use strict';

  angular
    .module('app.filters', [
      'app.filters.sanitize',
      'app.filters.truncate',
      'app.filters.orderObjectBy',
      'app.filters.strip_html',
      'app.filters.trustsrc',
      'app.filters.unique'
    ])
  ;
})(window.angular);
