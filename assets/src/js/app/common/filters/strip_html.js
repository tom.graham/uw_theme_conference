(function(angular) {
  'use strict';

  angular
    .module('app.filters.strip_html', [])
    .filter('strip_html', function() {
    return function(text) {
      return String(text).replace(/<[^>]+>/gm, '');
    }
  });
})(window.angular);