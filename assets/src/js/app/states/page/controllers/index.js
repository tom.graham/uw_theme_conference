(function(angular) {
  'use strict';

  angular
    .module('app.states.page.controllers', [
      'app.states.page.controllers.pageShowController'
    ])
  ;
})(window.angular);
