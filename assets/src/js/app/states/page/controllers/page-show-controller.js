(function(angular, _) {
  'use strict';

  angular
    .module('app.states.page.controllers.pageShowController', [])
    .controller('pageShowController', function pageShowController(ckeditorSocialMediaService, metaService, pageService, $scope, $stateParams, $timeout) {

      var vm = this;
      vm.page_loading = true;

      setInitialState();

      /**
       * Set the intial state of the controller.
       */
      function setInitialState() {
        vm.page = {};

        $scope.$watch('vm.page.body', function(newValue, oldValue) {
          if (newValue) {
            $timeout(function() {
              ckeditorSocialMediaService.parse();
            }, 300);
          }
        })

        var slug = ($stateParams.slug) ? $stateParams.slug : 'home';
        pageService.getOne(slug, handleGetPage);
      }

      function handleGetPage(data) {
        if (!data) {
          data = {
            error: {
              title: 'Page Not Found',
              message: 'The requested page could not be found.'
            }
          };
        } else {
          metaService.updateMetaTags(data.metatags);
        }

        vm.page = data;
        vm.page.sidebar = true;

        // By default, pages have the page feature.
        if(!vm.page.feature){
          vm.page.feature = 'page';
        }
        if(vm.page.display_wide_screen !== "1"){
           vm.page.sidebar = true;
        }
        else {
           vm.page.sidebar = false;
        }

        // Feature pages and homepage have no sidebar.
        if (vm.page.feature != 'page' || vm.page.home) {
          vm.page.sidebar = false;
        }

        if (vm.page.error) {
          vm.page.sidebar = true;
        }

        // Blog has no masthead.
        if (vm.page.feature == 'blog') {
          
          vm.page.masthead = false;
        }

        if (vm.page.rendered_html) {
          vm.originalFormHTML = null;
          $timeout(bindForm, 300);
        }

        vm.page_loading = false;
        $('.uw_video-embed').fitVids();
      }

      function bindForm() {
        $('.post-content form').on('submit', function(e) {
          e.preventDefault();

          var $form = $(this);

          var method   = $form.attr('method');
          var action   = $form.attr('action').replace('/api/v1.0/conference_web_forms/', '/');
          var payload  = $form.serialize();
          var success  = '<div class="form-success">Thank you for your submission</div>';

          if (!vm.originalFormHTML) {
            vm.originalFormHTML = $form.html();
          }

          $.ajax({
            type: method,
            url: action,
            data: payload,
            success: function(response) {
              var errors = $(response).find('.messages.error');
              if (errors && errors.length > 0) {
                var $new = $(response).find('.content_node form').html();
                $form.html($new);
              } else {
                $form.html(vm.originalFormHTML); // The HTML gets all mucked up with errors.
                $form.prepend(success);
                $form.trigger('reset');
              }
            }
          });
        });
      }
    })
  ;
})(window.angular, window._);
