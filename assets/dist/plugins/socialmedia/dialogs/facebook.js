(function() {
  // Variable used to track when the form is submitted.
  // i.e to check for double submissions on using enter key in dialog.
  var formSubmitted = false;
  var facebookDialog = function(editor) {
    return {
      title : 'Facebook Properties',
      minWidth : 625,
      minHeight : 100,
      contents: [{
        id: 'facebook',
        label: 'facebook',
        elements:[{
          type: 'text',
          id: 'facebookInput',
          label: 'Facebook page name: facebook.com/',
          setup: function(element) {
                   this.setValue(element.getAttribute('data-username'));
                 }
        },{
          type: 'text',
          id: 'displaynameInput',
          label: 'Display name:',
          setup: function(element) {
                   this.setValue(element.getAttribute('data-displayname'));
                 }
        }]
      }],
      onOk: function() {
        //get form information
        facebookInput = this.getValueOf('facebook','facebookInput');
        displaynameInput = this.getValueOf('facebook','displaynameInput');
        //validate input. Note that there is probably a CKEditor specific way to do this, but this works.
        errors = '';
        if (!CKEDITOR.socialmedia.facebook_username_regex_1.test(facebookInput) || !CKEDITOR.socialmedia.facebook_username_regex_2.test(facebookInput)) {
          errors += "You must enter a valid Facebook user name.\r\n";
        }
        if (!displaynameInput) {
          errors += "You must enter a display name.\r\n";
        }
				// If form has been submitted before then set it back to not being seeing before.
				// i.e if this is double submission set it back to not being run before
        if(formSubmitted == true) {
          formSubmitted = false;
          return false;
        }
				// Only display erros if there are errors to display and the form has not been run before.
        else if (errors && formSubmitted == false) {
          alert(errors);
          formSubmitted = true;
          return false;
        }
        else {
          //create the ckfacebook element
          var ckfacebookNode = new CKEDITOR.dom.element('ckfacebook');

          //save contents of dialog as attributes of the element
          ckfacebookNode.setAttribute('data-username',facebookInput);
          ckfacebookNode.setAttribute('data-displayname',displaynameInput);
          //adjust title based on user input
          CKEDITOR.lang.en.fakeobjects.ckfacebook = CKEDITOR.socialmedia.ckfacebook+': '+displaynameInput;
          //create the fake image for this element and insert into the document (realElement, className, realElementType, isResizable)
          var newFakeImage = editor.createFakeElement(ckfacebookNode, 'ckfacebook', 'ckfacebook', false);
          if (this.fakeImage) {
            newFakeImage.replace(this.fakeImage);
            editor.getSelection().selectElement(newFakeImage);
          } else {
            editor.insertElement(newFakeImage);
          }
          //reset title
          CKEDITOR.lang.en.fakeobjects.ckfacebook = CKEDITOR.socialmedia.ckfacebook;
        }
      },
      onShow: function() {
        this.fakeImage = this.ckfacebookNode = null;
        var fakeImage;

        // Check if element is right clicked or icon was clicked, if not use global varaible doubleclick_element
        if (this.getSelectedElement()) {
          fakeImage = this.getSelectedElement();
        }
        else {
          fakeImage = doubleclick_element;
        }

        if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'ckfacebook') {
          this.fakeImage = fakeImage;
          var ckfacebookNode = editor.restoreRealElement(fakeImage);
          this.ckfacebookNode = ckfacebookNode;
          this.setupContent(ckfacebookNode);
        }
      }
    }
  }

  CKEDITOR.dialog.add('facebook', function(editor) {
    return facebookDialog(editor);
  });    
})();
