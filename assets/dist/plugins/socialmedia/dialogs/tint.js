(function() {
  // Variable used to track when the form is submitted.
  // i.e to check for double submissions on using enter key in dialog.
  var formSubmitted = false;
  var tintDialog = function(editor) {
    return {
      title : 'Tint Properties',
      minWidth : 625,
      minHeight : 150,
      contents: [{
        id: 'tint',
        label: 'tint',
        elements:[{
          type: 'text',
          id: 'idInput',
          label: 'Tint ID: (required) ',
          setup: function(element) {
                   this.setValue(element.getAttribute('data-id'));
                 }
        },{
          type: 'text',
          id: 'keywordsInput',
          label: 'Keywords Filter: (optional) ',
          setup: function(element) {
                   this.setValue(element.getAttribute('data-url'));
                 }
        },{
          type: 'text',
          id: 'heightInput',
          label: 'Tint height in pixels (minimum 100): (required)',
          setup: function(element) {
                   this.setValue(element.getAttribute('data-height'));
                 }
        },{
          type: 'text',
          id: 'columnsInput',
          label: 'Number of columns: (leave blank to be responsive)',
          setup: function(element) {
                   this.setValue(element.getAttribute('data-columns'));
                 }
        }]
      }],
      onOk: function() {
        // Get form information.
        idInput = this.getValueOf('tint','idInput');
        keywordsInput = this.getValueOf('tint','keywordsInput');
        heightInput = this.getValueOf('tint','heightInput');
        columnsInput = this.getValueOf('tint','columnsInput');
        // Validate input. Note that there is probably a CKEditor specific way to do this, but this works.
        errors = '';
        if (!idInput) {
          errors += "You must enter the ID of the Tint visualization.\r\n";
        }
        if (!heightInput || heightInput.NaN || heightInput < 100 || Math.floor(heightInput) != heightInput) {
          errors += "You must enter a valid whole number for the height.\r\n";
        }
        if (Math.floor(columnsInput) != columnsInput) {
          errors += "You must enter a valid whole number for the columns.\r\n";
        }
        // If form has been submitted before then set it back to not being seeing before.
        // i.e if this is double submission set it back to not being run before
        if(formSubmitted == true) {
          formSubmitted = false;
          return false;
        }
        // Only display erros if there are errors to display and the form has not been run before.
        else if (errors && formSubmitted == false) {
          alert(errors);
          formSubmitted = true;
          return false;
        }
        else {
          // Create the cktint element.
          var cktintNode = new CKEDITOR.dom.element('cktint');
          // Save contents of dialog as attributes of the element.
          cktintNode.setAttribute('data-id',idInput);
          cktintNode.setAttribute('data-keywords',keywordsInput);
          cktintNode.setAttribute('data-height',heightInput);
          cktintNode.setAttribute('data-columns',columnsInput);
          // Adjust title based on user input.
          CKEDITOR.lang.en.fakeobjects.cktint = CKEDITOR.socialmedia.cktint + ': ' + idInput;
          // Create the fake image for this element and insert into the document (realElement, className, realElementType, isResizable).
          var newFakeImage = editor.createFakeElement(cktintNode, 'cktint', 'cktint', false);
          // Set the fake object to the entered height; if there isn't one, use 100 so it's not invisible.
          newFakeImage.$.height = heightInput;
          newFakeImage.addClass('cktint');
          if (this.fakeImage) {
            newFakeImage.replace(this.fakeImage);
            editor.getSelection().selectElement(newFakeImage);
          } else {
            editor.insertElement(newFakeImage);
          }
          // Reset title.
          CKEDITOR.lang.en.fakeobjects.cktint = CKEDITOR.socialmedia.cktint;
        }
      },
      onShow: function() {
        // Set up to handle existing items.
        this.fakeImage = this.cktintNode = null;
        var fakeImage;

        // Check if element is right clicked or icon was clicked, if not use global varaible doubleclick_element
        if (this.getSelectedElement()) {
          fakeImage = this.getSelectedElement();
        }
        else {
          fakeImage = doubleclick_element;
        }

        if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'cktint') {
          this.fakeImage = fakeImage;
          var cktintNode = editor.restoreRealElement(fakeImage);
          this.cktintNode = cktintNode;
          this.setupContent(cktintNode);
        }
      }
    }
  }

  CKEDITOR.dialog.add('tint', function(editor) {
    return tintDialog(editor);
  });    
})();
